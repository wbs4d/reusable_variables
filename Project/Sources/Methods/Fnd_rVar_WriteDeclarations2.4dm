//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: Fnd_rVar_WriteDeclarations2


// Method Type:    Protected

// Parameters:
C_LONGINT:C283($DeclareSingleVariableType_i; $1)
C_LONGINT:C283($StringVarLength_i; $2)
C_LONGINT:C283($VarChosenType_i; $3)
C_LONGINT:C283($NumberOfVars_i; $4)
C_LONGINT:C283($StartNumber_i; $5)
C_TEXT:C284($VariablePrefix_t; $6)
C_POINTER:C301($CompilerDeclarations_ptr; $7)

// Local Variables:
C_TEXT:C284($CR)
C_TEXT:C284($Alert_t)
C_LONGINT:C283($CurrentVar_i; $DataIndex_i; $DataType_i)
C_TEXT:C284($Prefix_s; $Suffix_s; $Delimiter_s)
C_LONGINT:C283($WindowID; $ProgressCounter_i; $MaxDeclarations_i; $NumberOfVariableTypes_i)

// Created by Wayne Stewart (Oct 23, 2007)
//     waynestewart@mac.com
//   
// ----------------------------------------------------


// ----------------------------------------------------
//   ASSIGN THE PARAMETERS
// ----------------------------------------------------
$DeclareSingleVariableType_i:=$1
$StringVarLength_i:=$2
$VarChosenType_i:=$3
$NumberOfVars_i:=$4
$StartNumber_i:=$5
$VariablePrefix_t:=$6
$CompilerDeclarations_ptr:=$7

// ----------------------------------------------------
//   INITIALISE THE LOCAL VARIABLES
// ----------------------------------------------------
$CR:=Char:C90(13)
$Prefix_s:="("
$Delimiter_s:="`------------------------------"
$ProgressCounter_i:=0
$NumberOfVariableTypes_i:=21

// ----------------------------------------------------
//   INITIALISE THE LOCAL ARRAYS
//   I'm just building these in another method to make this one easier to read!
// ----------------------------------------------------
ARRAY INTEGER:C220($VariableTypes_ai; 0)
ARRAY TEXT:C222($Prefix_at; 0)
ARRAY TEXT:C222($Suffix_at; 0)
Fnd_rVar_BuildArrays($StringVarLength_i; ->$VariableTypes_ai; ->$Prefix_at; ->$Suffix_at)

$WindowID:=Open form window:C675("MssgWindow"; Palette form window:K39:9; Horizontally centered:K39:1; Vertically centered:K39:4; *)
SET WINDOW TITLE:C213("Working...")


If ($DeclareSingleVariableType_i=1)
	$DataIndex_i:=Find in array:C230($VariableTypes_ai; $VarChosenType_i)
	$Prefix_s:=$Prefix_at{$DataIndex_i}
	$Suffix_s:=$Suffix_at{$DataIndex_i}
	$MaxDeclarations_i:=$NumberOfVars_i
	
	$CompilerDeclarations_ptr->:=$CR+"Fnd_rVar_VariableCount ("+String:C10($VarChosenType_i)+" ;"+String:C10($StartNumber_i+$NumberOfVars_i-1)+")"+$CR
	$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$Delimiter_s+($CR*2)
	
	For ($CurrentVar_i; $StartNumber_i; $StartNumber_i+$NumberOfVars_i-1)
		If ($CurrentVar_i>999)
			$CurrentVar_i:=$StartNumber_i+$NumberOfVars_i+1
		Else 
			If ($Prefix_s="C_INTEGER(")
			Else 
				$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$Prefix_s+$VariablePrefix_t+String:C10($VarChosenType_i)+"_"+String:C10($CurrentVar_i; "000")+$Suffix_s+$CR
				$ProgressCounter_i:=$ProgressCounter_i+1
				If ($ProgressCounter_i%100=0)
					ERASE WINDOW:C160($WindowID)
					GOTO XY:C161(2; 1)
					MESSAGE:C88("Declaration: "+String:C10($ProgressCounter_i; "###,##0")+" of "+String:C10($MaxDeclarations_i; "###,##0"))
				End if 
			End if 
		End if 
	End for 
	$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$CR+$Delimiter_s+($CR*2)
	
Else 
	$CompilerDeclarations_ptr->:=""
	$MaxDeclarations_i:=$NumberOfVars_i*20
	For ($DataIndex_i; 2; $NumberOfVariableTypes_i)  //  We ignore Alpha Field
		If ($DataIndex_i=14)  //  Integer
		Else 
			
			$DataType_i:=$VariableTypes_ai{$DataIndex_i}
			$Prefix_s:=$Prefix_at{$DataIndex_i}
			$Suffix_s:=$Suffix_at{$DataIndex_i}
			
			$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$CR+"Fnd_rVar_VariableCount ("+String:C10($DataType_i)+" ;"+String:C10($StartNumber_i+$NumberOfVars_i-1)+")"+$CR
			For ($CurrentVar_i; $StartNumber_i; $StartNumber_i+$NumberOfVars_i-1)
				If ($CurrentVar_i>999)
					$CurrentVar_i:=$StartNumber_i+$NumberOfVars_i+1
				Else 
					$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$Prefix_s+$VariablePrefix_t+String:C10($DataType_i)+"_"+String:C10($CurrentVar_i; "000")+$Suffix_s+$CR
					$ProgressCounter_i:=$ProgressCounter_i+1
					If ($ProgressCounter_i%100=0)
						ERASE WINDOW:C160($WindowID)
						GOTO XY:C161(2; 1)
						MESSAGE:C88("Declaration: "+String:C10($ProgressCounter_i; "###,##0")+" of "+String:C10($MaxDeclarations_i; "###,##0"))
					End if 
				End if 
				
			End for 
			$CompilerDeclarations_ptr->:=$CompilerDeclarations_ptr->+$CR+$Delimiter_s+($CR*2)
			
		End if 
	End for 
	
End if 

CLOSE WINDOW:C154($WindowID)

$CompilerDeclarations_ptr->:="`  Compiler Definitions automatically generated."+$CR+"`  Total # of characters: "+String:C10(Length:C16($CompilerDeclarations_ptr->)+117; "###,##0")+$CR+$Delimiter_s+($CR*2)+$CompilerDeclarations_ptr->
