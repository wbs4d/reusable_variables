//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ClearAll
// Global and IP variables accessed:     None Used

// Method Type:    Protected

// Parameters:    None Used

// Local Variables:
C_LONGINT:C283($CurrentVarType_i;$NumberOfVarTypes_i)
C_LONGINT:C283($CurrentVariable_i;$NumberOfVariables_i)


// Returns:    Nothing

// Created by Wayne Stewart (6 Apr, 2006)
//     waynestewart@mac.com
//  This method will clear all the variables in the
//    rVar system.
// ---------------------------------------------------

Fnd_rVar_Init

Fnd_rVar_LockStack

$NumberOfVarTypes_i:=Size of array:C274(<>Fnd_rVar_inUse_aBool)
For ($CurrentVarType_i;1;$NumberOfVarTypes_i)
	If (Fnd_rVar_IsValidVariableType($CurrentVarType_i))  //  Only do this if it is a valid type
		If (Fnd_rVar_VariableCount($CurrentVarType_i)>0)
			Fnd_rVar_ResetAllVariables($CurrentVarType_i)
		End if 
	End if 
End for 

//  At this stage all vars have been set to "zero"
//  now we declare them all available again
$NumberOfVarTypes_i:=Size of array:C274(<>Fnd_rVar_inUse_aBool)
For ($CurrentVarType_i;1;$NumberOfVarTypes_i)
	If (Fnd_rVar_IsValidVariableType($CurrentVarType_i))  //  Only do this if it is a valid type
		Get pointer:C304("<>Fnd_rVar_t"+String:C10($CurrentVarType_i)+"_Usage")->:=0
		$NumberOfVariables_i:=Size of array:C274(<>Fnd_rVar_inUse_aBool{$CurrentVarType_i})
		For ($CurrentVariable_i;1;$NumberOfVariables_i)
			<>Fnd_rVar_inUse_aBool{$CurrentVarType_i}{$CurrentVariable_i}:=False:C215
		End for 
		
	End if 
End for 

Fnd_rVar_UnlockStack

