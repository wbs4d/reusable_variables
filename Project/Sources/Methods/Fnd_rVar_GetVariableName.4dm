//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------

// Project Method: Fnd_rVar_Get_Variable_Name


// Method Type:     Protected


// Parameters: 

C_POINTER:C301($1)  //$1 is referenced variable


// Local Variables:  

C_POINTER:C301($Variable_Ptr)
C_TEXT:C284($VariableName_t)
C_LONGINT:C283($TableNumber_i)
C_LONGINT:C283($FieldNumber_i)

// Returns: 

C_TEXT:C284($0)  //$0 is name of variable


// Created by Wayne Stewart (18/04/2004)

//     waynestewart@mac.com

//   This component is based on the DSS library

//     first published by Deep Sky Technology on

//     the 4D Zine website.


// ----------------------------------------------------


$VariableName_t:=""
$Variable_Ptr:=$1

RESOLVE POINTER:C394($Variable_Ptr;$VariableName_t;$TableNumber_i;$FieldNumber_i)
$0:=$VariableName_t