//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_AdjustUsage ($variableType;$increment)

// Increments (or decrements) the counters for the different data types.

// Access: Private

// Parameters: 
//   $1 : Integer : The reusable variable type
//   $2 : Integer : Amount to increment (decrement) by

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com
// ----------------------------------------------------
If (False:C215)
	C_LONGINT:C283(Fnd_rVar_AdjustUsage; $1; $2)
End if 


C_LONGINT:C283($VariableType_i; $Increment_i)
C_POINTER:C301($Usage_ptr)

$VariableType_i:=$1
$Increment_i:=$2
$VariableType_i:=Fnd_rVar_ConvertType($VariableType_i)

$Usage_ptr:=Get pointer:C304("<>Fnd_rVar_t"+String:C10($VariableType_i)+"_Usage")

$Usage_ptr->:=$Usage_ptr->+$Increment_i

If ($Usage_ptr-><0)
	$Usage_ptr->:=0
End if 