//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_CrtVarDeclarations
// Global and IP variables accessed:     None Used

// Method Type:    Private

// Parameters:
C_LONGINT:C283($1)  //  Dummy

// Local Variables:     
C_LONGINT:C283($processNumber; $WindowID)

// Returns:    Nothing

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com

// Modified by: Wayne Stewart (23/10/07)
// 4D v11
//  No longer appropriate to run this in a host database

// ----------------------------------------------------

Fnd_rVar_Init

If (Count parameters:C259=0)
	$processNumber:=New process:C317("Fnd_rVar_CrtVarDeclarations"; 0; "$Create Variables"; 1; *)
	BRING TO FRONT:C326($processNumber)
Else 
	$WindowID:=Open form window:C675("CompilerDefinitions"; Palette form window:K39:9; On the right:K39:3; Vertically centered:K39:4; *)
	DIALOG:C40("CompilerDefinitions")
	CLOSE WINDOW:C154
End if 
