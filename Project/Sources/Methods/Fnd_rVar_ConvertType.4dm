//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ConvertType
// Global and IP variables accessed:     None Used

// Method Type:    Private

// Parameters:
C_LONGINT:C283($1)

// Local Variables:     None Used
C_LONGINT:C283($VariableType_i)

// Returns:    Nothing
C_LONGINT:C283($0)

// Created by Wayne Stewart (Aug 7, 2005)
//     waynestewart@mac.com
// ----------------------------------------------------

$VariableType_i:=$1

Case of 
	: ($VariableType_i=Is integer:K8:5)
		
		$VariableType_i:=Is longint:K8:6
		
	: ($VariableType_i=Is alpha field:K8:1) | \
		($VariableType_i=Is string var:K8:2)
		
		$VariableType_i:=Is text:K8:3
		
End case 

$0:=$VariableType_i