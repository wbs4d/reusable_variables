//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ArrAddElements

// Used to add elements to an array

// Access: Private

// Parameters: 
//   $1 : Pointer : Pointer to array to increase in size
//   $2 : Integer : The number of elements to add

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
// ----------------------------------------------------



If (False:C215)
	Fnd_rVar_ArrAddElements
End if 


C_POINTER:C301($1)  //  Pointer to array to increase in size
C_LONGINT:C283($2)  // The number of elements to add  

// Local Variables:  
C_POINTER:C301($Array_ptr)  //  
C_LONGINT:C283($NumberOfElements_i)
C_LONGINT:C283($VariableType_i)
C_LONGINT:C283($InitialSizeOfArray_i)
C_TEXT:C284($Exception_t)



$Array_ptr:=$1
$NumberOfElements_i:=$2
$VariableType_i:=Type:C295($1)

// Check for errors
$Exception_t:=""
Case of 
	: ($VariableType_i#Is pointer:K8:14)  //  #1  Only a pointer should be passed to this method. 
		//This error can only occur in interpreted mode
		$Exception_t:="Incorrect parameter type (not a pointer) passed."
		
	: (Not:C34(Fnd_rVar_IsAnArray(Type:C295($Array_ptr->))))  //  #2  If the pointer is not to an array then we have a stuff up
		$Exception_t:="Incorrect pointer type (not an array pointer) passed."
		
	: ($NumberOfElements_i<0)  //  #3  A negative number of elements were specified: You should be using the Remove method
		$Exception_t:="Negative number of elements specified."
End case 


If (Length:C16($Exception_t)=0)
	$InitialSizeOfArray_i:=Size of array:C274($Array_ptr->)
	INSERT IN ARRAY:C227($Array_ptr->; $InitialSizeOfArray_i+1; $NumberOfElements_i)  //add the element(s)  
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_ArrAddElements"; $Exception_t)
End if 