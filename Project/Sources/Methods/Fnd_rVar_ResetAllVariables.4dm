//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ResetAllVariables

// Method Type:     Private

// Parameters: 
C_LONGINT:C283($1)  //  This is the variable type

// Local Variables:  
C_LONGINT:C283($LoopCounter_i; $NumberOfVariables_i; $VariableType_i)
C_POINTER:C301($reusableVariable_ptr)
C_TEXT:C284($reusableVariableName_t)
C_TEXT:C284($Exception_t)

// Returns:     Nothing

// Created by Wayne Stewart (14/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

$VariableType_i:=$1

//  Error Checking
Case of 
	: (Not:C34(Fnd_rVar_IsValidVariableType($VariableType_i)))
		$Exception_t:="Unknown variable type: "+String:C10($VariableType_i)
		
End case 

If (Length:C16($Exception_t)=0)
	
	$NumberOfVariables_i:=Fnd_rVar_VariableCount($1)
	
	For ($LoopCounter_i; 1; $NumberOfVariables_i)
		$reusableVariableName_t:=Fnd_rVar_BuildVariableName($VariableType_i; $LoopCounter_i)
		$reusableVariable_ptr:=Get pointer:C304($reusableVariableName_t)
		Fnd_rVar_ResetValue($reusableVariable_ptr)
	End for 
	
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_ResetAllVariables"; $Exception_t)
	
End if 