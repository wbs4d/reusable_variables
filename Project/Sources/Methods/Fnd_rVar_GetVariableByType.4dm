//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_GetVariableByType

// Method Type:     Protected

// Parameters: 
C_LONGINT:C283($1)

// Local Variables:  
C_LONGINT:C283($VariableType_i)
C_LONGINT:C283($VariableIndex_i)

// Returns: 
C_POINTER:C301($0)  //   The pointer to the returned variable

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// Description
//$0 is pointer to variable
//   = ◊Fnd_rVar_POINTER_BLANK if none available
//$1 is type of variable wanted
// ----------------------------------------------------


$VariableType_i:=$1

Fnd_rVar_Init


If (Fnd_rVar_IsValidVariableType($VariableType_i))  //  Check to see if this a valid type
	
	$VariableType_i:=Fnd_rVar_ConvertType($VariableType_i)
	
	If (Size of array:C274(<>Fnd_rVar_inUse_aBool{$VariableType_i})#0)  //make sure there are some variables of this type
		
		Fnd_rVar_LockStack  //lock the DSS stack
		
		$VariableIndex_i:=Find in array:C230(<>Fnd_rVar_inUse_aBool{$VariableType_i};False:C215;1)  //find an unused variable matching type    
		If ($VariableIndex_i#-1)
			
			<>Fnd_rVar_inUse_aBool{$VariableType_i}{$VariableIndex_i}:=True:C214  //flag variable as in use    
			
			$0:=Get pointer:C304(Fnd_rVar_BuildVariableName($VariableType_i;$VariableIndex_i))  //set return value
			Fnd_rVar_AdjustUsage($VariableType_i;1)  //  Increments the usage counter for this data type
			//
		Else 
			Fnd_rVar_BugAlert("Fnd_rVar_GetVariableByType";"No more variables of type "+String:C10($VariableType_i)+" are available.")
			$0:=<>Fnd_rVar_Blank_Ptr
			
		End if 
		
		Fnd_rVar_UnlockStack  //unlock the DSS stack
		
	Else 
		Fnd_rVar_BugAlert("Fnd_rVar_GetVariableByType";"No variables of type "+String:C10($VariableType_i)+" are available.")
		$0:=<>Fnd_rVar_Blank_Ptr
		
	End if 
	//
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_Get_VariableByType";"Invalid data type: "+String:C10($VariableType_i))
	$0:=<>Fnd_rVar_Blank_Ptr
	
End if 
