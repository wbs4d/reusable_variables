//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_VariableCount

// Method Type:     Private

// Parameters:    
C_LONGINT:C283($1;$2)

// Local Variables:  
C_LONGINT:C283($ParameterCount_i;$VariableType_i;$VariableCount_i)

// Returns: 
C_LONGINT:C283($0)

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.
// Modified by: wbs (17/5/04)
//  Implemented new exception catching method.
// ----------------------------------------------------

$ParameterCount_i:=Count parameters:C259
$VariableType_i:=$1
$VariableCount_i:=-1

//  Error Checking
C_TEXT:C284($Exception_t)
Case of 
	: (Not:C34(Fnd_rVar_IsValidVariableType($VariableType_i)))
		$Exception_t:="Unknown variable type: "+String:C10($VariableType_i)
		
End case 


If (Length:C16($Exception_t)=0)  // No errors, OK to proceed.
	
	$VariableType_i:=Fnd_rVar_ConvertType($VariableType_i)
	
	If ($ParameterCount_i=2)  //   Set & return the variable count
		$VariableCount_i:=$2
		//If ($VariableCount_i><>Fnd_rVar_TypeCount_ai{$VariableType_i})
		<>Fnd_rVar_TypeCount_ai{$VariableType_i}:=$VariableCount_i
		//End if 
	Else   //  Just return the variable count
		$VariableCount_i:=<>Fnd_rVar_TypeCount_ai{$VariableType_i}
	End if 
	
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_VariableCount";$Exception_t)
End if 

Get pointer:C304("<>Fnd_rVar_t"+String:C10($VariableType_i)+"_Count")->:=$VariableCount_i
$0:=$VariableCount_i