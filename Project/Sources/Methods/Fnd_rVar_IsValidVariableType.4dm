//%attributes = {"invisible":true}
// ----------------------------------------------------

// Project Method: Fnd_rVar_IsValidVariableType


// Method Type:     Private


// Parameters: 

C_LONGINT:C283($1)  //$1 is type value to check


// Returns: 

C_BOOLEAN:C305($0)  //  True = Pointer is to an array


// Created by Wayne Stewart (18/04/2004)

//     waynestewart@mac.com

//   This component is based on the DSS library

//     first published by Deep Sky Technology on

//     the 4D Zine website.


// ----------------------------------------------------



$0:=(Find in array:C230(<>Fnd_VariableTypes_ai;$1;1)#-1)