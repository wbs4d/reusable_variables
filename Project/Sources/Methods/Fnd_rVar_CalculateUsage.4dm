//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_CalculateUsage

// Method Type:    Private

// Parameters:
C_LONGINT:C283($1; $VariableType_i)

// Local Variables:
C_LONGINT:C283($TotalUsed_i)
C_LONGINT:C283($TotalAvailable_i)
C_LONGINT:C283($PercentUsed_i)
C_POINTER:C301($Thermometer)
C_TEXT:C284($VarName_st7)

// Created by Wayne Stewart (Oct 23, 2007)
//     waynestewart@mac.com
// 
//   
// 
// ----------------------------------------------------

$VariableType_i:=<>Fnd_VariableTypes_ai{$1}
$VariableType_i:=Fnd_rVar_ConvertType($VariableType_i)


If ($VariableType_i>0)  //  we don't handle 0 = Alpha Field
	$TotalUsed_i:=Get pointer:C304("<>Fnd_rVar_t"+String:C10($VariableType_i)+"_Usage")->
	$TotalAvailable_i:=Get pointer:C304("<>Fnd_rVar_t"+String:C10($VariableType_i)+"_Count")->  // Modified by: Wayne Stewart (28/11/08), got rid of method call
	
	//`Fnd_rVar_VariableCount ($VariableType_i)
	$PercentUsed_i:=Round:C94($TotalUsed_i/$TotalAvailable_i*100; 0)
	//$Thermometer:=Get pointer("<>Fnd_rVar_t"+String($VariableType_i)+"_Thermo")
	$Thermometer:=OBJECT Get pointer:C1124(Object named:K67:5; "Fnd_rVar_t"+String:C10($VariableType_i)+"_Thermo")
	
	$Thermometer->:=$PercentUsed_i
	
	$VarName_st7:="@_t"+String:C10($VariableType_i)+"_@"
	Case of 
		: ($TotalUsed_i=0)
			OBJECT SET RGB COLORS:C628(*; $VarName_st7; 0x0000; -2)
			OBJECT SET FONT STYLE:C166(*; $VarName_st7; Plain:K14:1)
			
		: ($PercentUsed_i<70)
			OBJECT SET RGB COLORS:C628(*; $VarName_st7; 0x8000; -2)
			OBJECT SET FONT STYLE:C166(*; $VarName_st7; Underline:K14:4)
			
		: ($PercentUsed_i<90)
			OBJECT SET RGB COLORS:C628(*; $VarName_st7; 0x00FF9342; -2)
			OBJECT SET FONT STYLE:C166(*; $VarName_st7; Italic:K14:3+Underline:K14:4)
			//FONT STYLE(*;"Label_t"+String($VariableType_i)+"_";italic+Underline )
		Else 
			OBJECT SET RGB COLORS:C628(*; $VarName_st7; 0x00FF0000; -2)
			OBJECT SET FONT STYLE:C166(*; $VarName_st7; Bold:K14:2+Underline:K14:4)
			
	End case 
	
	
End if 