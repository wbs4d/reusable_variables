//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_Init

//  Initialises the rVar module
//  This component is based on the DSS library
//  first published by Deep Sky Technology on
//  the 4D Zine website.

// Access: Private


// Created by Wayne Stewart (12/04/2004)
//     waynestewart@mac.com
// ----------------------------------------------------

var $LoopCounter_i : Integer

C_BOOLEAN:C305(<>Fnd_rVar_Initialised_b)
If (Not:C34(<>Fnd_rVar_Initialised_b))
	Compiler_Fnd_rVar
	Fnd_rVar_BuildVariableCheckArr
	Compiler_Fnd_ReusableVariables
	
	Fnd_rVar_ArrSetSize(-><>Fnd_rVar_inUse_aBool; Size of array:C274(<>Fnd_rVar_TypeCount_ai))
	For ($LoopCounter_i; 1; Size of array:C274(<>Fnd_rVar_inUse_aBool); 1)
		If (Fnd_rVar_IsValidVariableType($LoopCounter_i))  //  Only do this if it is a valid type
			Fnd_rVar_ArrSetSize(-><>Fnd_rVar_inUse_aBool{$LoopCounter_i}; Fnd_rVar_VariableCount($LoopCounter_i))
			If (Fnd_rVar_VariableCount($LoopCounter_i)>0)
				Fnd_rVar_ResetAllVariables($LoopCounter_i)
			End if 
		End if 
	End for 
	
	
	
	<>Fnd_rVar_Initialised_b:=True:C214
	
	//  FYI:
	//   Fnd_rVar_StackSize is 128*1024
	//   Fnd_rVar_StackSemaphore is "$Fnd_rVar_Stack_Semaphore"
	
	ARRAY TEXT:C222($InstalledComponents_at; 0)
	COMPONENT LIST:C1001($InstalledComponents_at)
	
	<>Fnd_rVar_Foundation_Present_b:=Find in array:C230($InstalledComponents_at; "foundation@")>0
	
End if 