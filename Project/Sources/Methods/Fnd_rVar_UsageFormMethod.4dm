//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_UsageFormMethod
// Global and IP variables accessed:     None Used

// Method Type:    Private

// Parameters:    None Used

// Local Variables:     
C_LONGINT:C283($CurrentDataType_i)
C_BOOLEAN:C305($Fnd_Wnd_CloseNow_b;<>Fnd_rVar_Foundation_Present_b;$Fnd_Gen_QuitNow_b)

// Returns:    Nothing

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com
// ----------------------------------------------------



Case of 
	: (Form event code:C388=On Load:K2:1)
		<>Fnd_rVar_UpdateFreq_r:=5
		<>Fnd_rVar_SliderHelp_t:="Update frequency in seconds."+Char:C90(13)+"Currently set at "+String:C10(<>Fnd_rVar_UpdateFreq_r)+" second."
		SET TIMER:C645(Round:C94(<>Fnd_rVar_UpdateFreq_r*60;0))
		SET WINDOW TITLE:C213("Reusable Variables Usage")
		
	: (Form event code:C388=On Timer:K2:25)
		For ($CurrentDataType_i;1;21)
			Fnd_rVar_CalculateUsage($CurrentDataType_i)
		End for 
		REDRAW WINDOW:C456
		
	: (Form event code:C388=On Close Box:K2:21)
		CANCEL:C270
		
	: (<>Fnd_rVar_Foundation_Present_b)
		EXECUTE METHOD:C1007("Fnd_Wnd_CloseNow";$Fnd_Wnd_CloseNow_b)
		If ($Fnd_Wnd_CloseNow_b)
			CANCEL:C270
		Else 
			EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$Fnd_Gen_QuitNow_b)
			If ($Fnd_Gen_QuitNow_b)
				CANCEL:C270
			End if 
		End if 
		
		
End case 