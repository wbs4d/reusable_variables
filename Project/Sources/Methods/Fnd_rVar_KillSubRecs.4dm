//%attributes = {"invisible":true}
// ----------------------------------------------------

// Project Method: Fnd_rVar_KillSubRecs

// Global and IP variables accessed:     None Used


// Method Type:    Private


// Parameters:    None Used

C_POINTER:C301($1)
// Local Variables:     None Used


// Returns:    Nothing


// Created by Wayne Stewart (11/09/2004)

//     waynestewart@mac.com

// ----------------------------------------------------





_O_ALL SUBRECORDS:C109($1->)
While (_O_Records in subselection:C7($1->)>0)
	_O_DELETE SUBRECORD:C96($1->)
	_O_ALL SUBRECORDS:C109($1->)
End while 