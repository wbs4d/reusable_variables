//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_Null_Set_Variables

// Method Type:     Private

// Parameters:   
C_POINTER:C301(${1})

// Local Variables:      None Used
C_LONGINT:C283($LoopCounter_i)

// Returns:     Nothing

// Created by Wayne Stewart (14/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

For ($LoopCounter_i;1;Count parameters:C259;1)
	If (Type:C295(${$LoopCounter_i}->)=Is pointer:K8:14)
		${$LoopCounter_i}->:=<>Fnd_rVar_Blank_Ptr
	Else 
		Fnd_rVar_ResetValue(${$LoopCounter_i})
	End if 
End for 