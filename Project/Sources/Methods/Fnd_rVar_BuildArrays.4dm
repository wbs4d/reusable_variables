//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_BuildArrays

// One of the utility methods used in the creation of declarations
// results are returned by the three array pointers

// Access: Private

// Parameters: 
//   $1 : Integer : String Length
//   $2 : Pointer : Variable Types Array 
//   $3 : Pointer : Prefix Array
//   $4 : Pointer : Suffix Array

// Created by Wayne Stewart (14/5/04)

//     waynestewart@mac.com
// ----------------------------------------------------


// Parameters:    None Used
C_LONGINT:C283($StringVarLength_i; $1)
C_POINTER:C301($VariableTypesArray_ptr; $2)
C_POINTER:C301($PrefixArray_ptr; $3)
C_POINTER:C301($SuffixArray_ptr; $4)

// Local Variables:
C_TEXT:C284($UnarySuffix_t)
C_TEXT:C284($ArraySuffix_t)
C_LONGINT:C283($CurrentVar_i; $NumberOfVariableTypes_i)


// Modified by: Wayne Stewart (23/10/07)
// 4D v11 SQL version

// ----------------------------------------------------

$StringVarLength_i:=$1
$VariableTypesArray_ptr:=$2
$PrefixArray_ptr:=$3
$SuffixArray_ptr:=$4

$ArraySuffix_t:=";0)"
$UnarySuffix_t:=")"
$NumberOfVariableTypes_i:=21

ARRAY INTEGER:C220($VariableTypesArray_ptr->; $NumberOfVariableTypes_i)
ARRAY TEXT:C222($PrefixArray_ptr->; $NumberOfVariableTypes_i)
ARRAY TEXT:C222($SuffixArray_ptr->; $NumberOfVariableTypes_i)  //  Set up the suffix array

$VariableTypesArray_ptr->{1}:=Is alpha field:K8:1
$VariableTypesArray_ptr->{2}:=Boolean array:K8:21
$VariableTypesArray_ptr->{3}:=Date array:K8:20
$VariableTypesArray_ptr->{4}:=Integer array:K8:18
$VariableTypesArray_ptr->{5}:=LongInt array:K8:19
$VariableTypesArray_ptr->{6}:=Picture array:K8:22
$VariableTypesArray_ptr->{7}:=Pointer array:K8:23
$VariableTypesArray_ptr->{8}:=Real array:K8:17
$VariableTypesArray_ptr->{9}:=String array:K8:15
$VariableTypesArray_ptr->{10}:=Text array:K8:16
$VariableTypesArray_ptr->{11}:=Is BLOB:K8:12
$VariableTypesArray_ptr->{12}:=Is boolean:K8:9
$VariableTypesArray_ptr->{13}:=Is date:K8:7
$VariableTypesArray_ptr->{14}:=Is integer:K8:5
$VariableTypesArray_ptr->{15}:=Is longint:K8:6
$VariableTypesArray_ptr->{16}:=Is picture:K8:10
$VariableTypesArray_ptr->{17}:=Is pointer:K8:14
$VariableTypesArray_ptr->{18}:=Is real:K8:4
$VariableTypesArray_ptr->{19}:=Is string var:K8:2
$VariableTypesArray_ptr->{20}:=Is text:K8:3
$VariableTypesArray_ptr->{$NumberOfVariableTypes_i}:=Is time:K8:8

$PrefixArray_ptr->{1}:="C_TEXT("  //"C_STRING("+String($StringVarLength_i)+";"  //Is Alpha Field 
$PrefixArray_ptr->{2}:="ARRAY BOOLEAN("  //Boolean array 
$PrefixArray_ptr->{3}:="ARRAY DATE("  //Date array 
$PrefixArray_ptr->{4}:="ARRAY INTEGER("  //Integer array 
$PrefixArray_ptr->{5}:="ARRAY LONGINT("  //LongInt array 
$PrefixArray_ptr->{6}:="ARRAY PICTURE("  //Picture array 
$PrefixArray_ptr->{7}:="ARRAY POINTER("  //Pointer array 
$PrefixArray_ptr->{8}:="ARRAY REAL("  //Real array 
$PrefixArray_ptr->{9}:="ARRAY TEXT("  //"ARRAY STRING("+String($StringVarLength_i)+";"  //String array 
$PrefixArray_ptr->{10}:="ARRAY TEXT("  //Text array 
$PrefixArray_ptr->{11}:="C_BLOB("  //Is BLOB 
$PrefixArray_ptr->{12}:="C_BOOLEAN("  //Is Boolean 
$PrefixArray_ptr->{13}:="C_DATE("  //Is Date 
$PrefixArray_ptr->{14}:="C_LONGINT("  //"C_INTEGER("  //Is Integer 
$PrefixArray_ptr->{15}:="C_LONGINT("  //Is LongInt 
$PrefixArray_ptr->{16}:="C_PICTURE("  //Is Picture 
$PrefixArray_ptr->{17}:="C_POINTER("  //Is Pointer 
$PrefixArray_ptr->{18}:="C_REAL("  //Is Real 
$PrefixArray_ptr->{19}:="C_TEXT("  //"C_STRING("+String($StringVarLength_i)+";"  //Is String Var 
$PrefixArray_ptr->{20}:="C_TEXT("  //Is Text 
$PrefixArray_ptr->{$NumberOfVariableTypes_i}:="C_TIME("  //Is Time 

For ($CurrentVar_i; 1; $NumberOfVariableTypes_i)
	Case of 
		: ($VariableTypesArray_ptr->{$CurrentVar_i}=Is BLOB:K8:12)
			$SuffixArray_ptr->{$CurrentVar_i}:=$UnarySuffix_t
			
		: ($VariableTypesArray_ptr->{$CurrentVar_i}=Is pointer:K8:14)
			$SuffixArray_ptr->{$CurrentVar_i}:=$UnarySuffix_t
			
		: ($VariableTypesArray_ptr->{$CurrentVar_i}=Is string var:K8:2)
			$SuffixArray_ptr->{$CurrentVar_i}:=$UnarySuffix_t
			
		: ($VariableTypesArray_ptr->{$CurrentVar_i}<=Is time:K8:8)
			$SuffixArray_ptr->{$CurrentVar_i}:=$UnarySuffix_t
			
		Else 
			$SuffixArray_ptr->{$CurrentVar_i}:=$ArraySuffix_t
			
	End case 
End for 


//  I don't think these are needed but I won't delete them yet to save typing
// ARRAY INTEGER($Fnd_rVar2_ArrayTypes_ai;10)
//$Fnd_rVar2_ArrayTypes_ai{1}:=Array 2D 
//$Fnd_rVar2_ArrayTypes_ai{2}:=Boolean array 
//$Fnd_rVar2_ArrayTypes_ai{3}:=Date array 
//$Fnd_rVar2_ArrayTypes_ai{4}:=Integer array 
//$Fnd_rVar2_ArrayTypes_ai{5}:=LongInt array 
//$Fnd_rVar2_ArrayTypes_ai{6}:=Picture array 
//$Fnd_rVar2_ArrayTypes_ai{7}:=Pointer array 
//$Fnd_rVar2_ArrayTypes_ai{8}:=Real array 
//$Fnd_rVar2_ArrayTypes_ai{9}:=String array 
//$Fnd_rVar2_ArrayTypes_ai{10}:=Text array 


