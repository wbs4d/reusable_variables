//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_Unlock_Stack

// Method Type:     Private

// Parameters:     None Used

// Local Variables:      None Used

// Returns:     Nothing

// Created by Wayne Stewart (14/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

CLEAR SEMAPHORE:C144(Fnd_rVar_StackSemaphore)