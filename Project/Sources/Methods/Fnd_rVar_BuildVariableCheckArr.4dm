//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_BuildVariableCheckArr

// Creates IP array with allowable types

// Access: Private

// Created by Wayne Stewart (14/5/04)

//     waynestewart@mac.com
// ----------------------------------------------------

ARRAY INTEGER:C220(<>Fnd_VariableTypes_ai; 21)
ARRAY INTEGER:C220(<>Fnd_ArrayTypes_ai; 10)

<>Fnd_VariableTypes_ai{1}:=Is alpha field:K8:1
<>Fnd_VariableTypes_ai{2}:=Boolean array:K8:21
<>Fnd_VariableTypes_ai{3}:=Date array:K8:20
<>Fnd_VariableTypes_ai{4}:=Integer array:K8:18
<>Fnd_VariableTypes_ai{5}:=LongInt array:K8:19
<>Fnd_VariableTypes_ai{6}:=Picture array:K8:22
<>Fnd_VariableTypes_ai{7}:=Pointer array:K8:23
<>Fnd_VariableTypes_ai{8}:=Real array:K8:17
<>Fnd_VariableTypes_ai{9}:=String array:K8:15
<>Fnd_VariableTypes_ai{10}:=Text array:K8:16
<>Fnd_VariableTypes_ai{11}:=Is BLOB:K8:12
<>Fnd_VariableTypes_ai{12}:=Is boolean:K8:9
<>Fnd_VariableTypes_ai{13}:=Is date:K8:7
<>Fnd_VariableTypes_ai{14}:=Is integer:K8:5
<>Fnd_VariableTypes_ai{15}:=Is longint:K8:6
<>Fnd_VariableTypes_ai{16}:=Is picture:K8:10
<>Fnd_VariableTypes_ai{17}:=Is pointer:K8:14
<>Fnd_VariableTypes_ai{18}:=Is real:K8:4
<>Fnd_VariableTypes_ai{19}:=Is string var:K8:2
<>Fnd_VariableTypes_ai{20}:=Is text:K8:3
<>Fnd_VariableTypes_ai{21}:=Is time:K8:8

<>Fnd_ArrayTypes_ai{1}:=Array 2D:K8:24
<>Fnd_ArrayTypes_ai{2}:=Boolean array:K8:21
<>Fnd_ArrayTypes_ai{3}:=Date array:K8:20
<>Fnd_ArrayTypes_ai{4}:=Integer array:K8:18
<>Fnd_ArrayTypes_ai{5}:=LongInt array:K8:19
<>Fnd_ArrayTypes_ai{6}:=Picture array:K8:22
<>Fnd_ArrayTypes_ai{7}:=Pointer array:K8:23
<>Fnd_ArrayTypes_ai{8}:=Real array:K8:17
<>Fnd_ArrayTypes_ai{9}:=String array:K8:15
<>Fnd_ArrayTypes_ai{10}:=Text array:K8:16
