//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_WriteDeclarations
// Global and IP variables accessed:     None Used

// Method Type:    Private

// Parameters:    None Used

// Local Variables:
C_TEXT:C284($CR)
C_TEXT:C284($Alert_t)
C_LONGINT:C283($CurrentVar_i; $DataIndex_i; $DataType_i)
C_TEXT:C284($Prefix_s; $UnarySuffix_s; $ArraySuffix_s; $Suffix_s; $Delimiter_s)
C_LONGINT:C283($WindowID; $ProgressCounter_i; $MaxDeclarations_i; $NumberOfVariableTypes_i)

// Returns:    Nothing

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com


// Modified by: Wayne Stewart (28/11/08)
//   Now calls another component to create declarations - so it can run compiled?

// ----------------------------------------------------


If (True:C214)
	
	If ((Length:C16(<>Fnd_rVar_Prefix_t)>0) & ((Fnd_rVar_IsValidVariableType(<>Fnd_rVar_ChosenType_i) & (<>Fnd_rVar_ChosenType_i>0)) | (<>xb2=1)))  //  I can't believe I just wrote this!
		
		//C_LONGINT($DeclareSingleVariableType_i;$1)
		//C_LONGINT($StringVarLength_i;$2)
		//C_LONGINT($VarChosenType_i;$3)
		//C_LONGINT($NumberOfVars_i;$4)
		//C_LONGINT($StartNumber_i;$5)
		//C_TEXT($VariablePrefix_t;$6)
		//C_POINTER($CompilerDeclarations_ptr;$7)
		
		Fnd_rVar_WriteDeclarations2(<>xb1; <>Fnd_rVar_StrLength_i; <>Fnd_rVar_ChosenType_i; <>Fnd_rVar_NumberOfVars_i; <>Fnd_rVar_StartNumber_i; <>Fnd_rVar_Prefix_t; -><>Fnd_rVar_Declarations_t)
		
		
		
		
	Else 
		ALERT:C41("Please recheck your settings."+$CR+"Make certain at least one radio button is selected and you have entered a prefix.")
	End if 
	
	
	
	
	
Else 
	
	
	
	// ` ----------------------------------------------------
	//  ` Project Method: Fnd_rVar_WriteDeclarations
	//  ` Global and IP variables accessed:     None Used
	//
	//  ` Method Type:    Private
	//
	//  ` Parameters:    None Used
	//
	//  ` Local Variables:
	//C_STRING(1;$CR)
	//C_TEXT($Alert_t)
	//C_LONGINT($CurrentVar_i;$DataIndex_i;$DataType_i)
	//C_STRING(31;$Prefix_s;$UnarySuffix_s;$ArraySuffix_s;$Suffix_s;$Delimiter_s)
	//C_LONGINT($WindowID;$ProgressCounter_i;$MaxDeclarations_i;$NumberOfVariableTypes_i)
	//
	//  ` Returns:    Nothing
	//
	//  ` Created by Wayne Stewart (11/09/2004)
	//  `     waynestewart@mac.com
	//  ` ----------------------------------------------------
	//
	//
	//
	//
	//$CR:=Char(13)
	//$Prefix_s:="("
	//$ArraySuffix_s:=";0)"
	//$UnarySuffix_s:=")"
	//$Delimiter_s:="`------------------------------"
	//$ProgressCounter_i:=0
	//$NumberOfVariableTypes_i:=21
	//
	//ARRAY TEXT($Prefix_at;$NumberOfVariableTypes_i)
	//
	//$Prefix_at{1}:="C_STRING("+String(◊Fnd_rVar_StrLength_i)+";"  `Is Alpha Field 
	//$Prefix_at{2}:="ARRAY BOOLEAN("  `Boolean array 
	//$Prefix_at{3}:="ARRAY DATE("  `Date array 
	//$Prefix_at{4}:="ARRAY INTEGER("  `Integer array 
	//$Prefix_at{5}:="ARRAY LONGINT("  `LongInt array 
	//$Prefix_at{6}:="ARRAY PICTURE("  `Picture array 
	//$Prefix_at{7}:="ARRAY POINTER("  `Pointer array 
	//$Prefix_at{8}:="ARRAY REAL("  `Real array 
	//$Prefix_at{9}:="ARRAY STRING("+String(◊Fnd_rVar_StrLength_i)+";"  `String array 
	//$Prefix_at{10}:="ARRAY TEXT("  `Text array 
	//$Prefix_at{11}:="C_BLOB("  `Is BLOB 
	//$Prefix_at{12}:="C_BOOLEAN("  `Is Boolean 
	//$Prefix_at{13}:="C_DATE("  `Is Date 
	//$Prefix_at{14}:="C_INTEGER("  `Is Integer 
	//$Prefix_at{15}:="C_LONGINT("  `Is LongInt 
	//$Prefix_at{16}:="C_PICTURE("  `Is Picture 
	//$Prefix_at{17}:="C_POINTER("  `Is Pointer 
	//$Prefix_at{18}:="C_REAL("  `Is Real 
	//$Prefix_at{19}:="C_STRING("+String(◊Fnd_rVar_StrLength_i)+";"  `Is String Var 
	//$Prefix_at{20}:="C_TEXT("  `Is Text 
	//$Prefix_at{21}:="C_TIME("  `Is Time 
	//
	//
	//ARRAY TEXT($Suffix_at;$NumberOfVariableTypes_i)
	//For ($CurrentVar_i;1;$NumberOfVariableTypes_i)
	//Case of 
	//: (◊Fnd_VariableTypes_ai{$CurrentVar_i}=Is BLOB )
	//$Suffix_at{$CurrentVar_i}:=$UnarySuffix_s
	//
	//: (◊Fnd_VariableTypes_ai{$CurrentVar_i}=Is Pointer )
	//$Suffix_at{$CurrentVar_i}:=$UnarySuffix_s
	//
	//: (◊Fnd_VariableTypes_ai{$CurrentVar_i}=Is String Var )
	//$Suffix_at{$CurrentVar_i}:=$UnarySuffix_s
	//
	//
	//: (◊Fnd_VariableTypes_ai{$CurrentVar_i}<=Is Time )
	//$Suffix_at{$CurrentVar_i}:=$UnarySuffix_s
	//
	//Else 
	//$Suffix_at{$CurrentVar_i}:=$ArraySuffix_s
	//
	//End case 
	//End for 
	//
	//
	//
	//If ((Length(◊Fnd_rVar_Prefix_t)>0) & ((Fnd_rVar_IsValidVariableType (◊Fnd_rVar_ChosenType_i) & (◊Fnd_rVar_ChosenType_i>0)) | (◊xb2=1)))  `  I can't believe I just wrote this!
	//
	//$WindowID:=Open form window("MssgWindow";Palette form window ;Horizontally Centered ;Vertically Centered ;*)
	//
	//SET WINDOW TITLE("Working...")
	//
	//If (◊xb1=1)
	//
	//$DataIndex_i:=Find in array(◊Fnd_VariableTypes_ai;◊Fnd_rVar_ChosenType_i)
	//$Prefix_s:=$Prefix_at{$DataIndex_i}
	//$Suffix_s:=$Suffix_at{$DataIndex_i}
	//$MaxDeclarations_i:=◊Fnd_rVar_NumberOfVars_i
	//
	//◊Fnd_rVar_Declarations_t:=$CR+"Fnd_rVar_VariableCount ("+String(◊Fnd_rVar_ChosenType_i)+" ;"+String(◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i-1)+")"+$CR
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$Delimiter_s+($CR*2)
	//For ($CurrentVar_i;◊Fnd_rVar_StartNumber_i;◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i-1)
	//If ($CurrentVar_i>999)
	//$CurrentVar_i:=◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i+1
	//Else 
	//If ($Prefix_s="C_INTEGER(")
	//Else 
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$Prefix_s+◊Fnd_rVar_Prefix_t+String(◊Fnd_rVar_ChosenType_i)+"_"+String($CurrentVar_i;"000")+$Suffix_s+$CR
	//$ProgressCounter_i:=$ProgressCounter_i+1
	//ERASE WINDOW($WindowID)
	//GOTO XY(2;1)
	//MESSAGE("Declaration: "+String($ProgressCounter_i;"###,##0")+" of "+String($MaxDeclarations_i;"###,##0"))
	//End if 
	//End if 
	//
	//If (Length(◊Fnd_rVar_Declarations_t)>31800)
	//If (◊Fnd_rVar_TextOrBlob_i=1)
	//$Alert_t:="32,000 characters is the most a text variable can hold."+$CR
	//$Alert_t:=$Alert_t+String(Length(◊Fnd_rVar_Declarations_t);"###,##0")
	//$Alert_t:=$Alert_t+" characters have been written so far, a total of "
	//$Alert_t:=$Alert_t+String($CurrentVar_i-◊Fnd_rVar_StartNumber_i;"###,##0")+" variable declarations."
	//$Alert_t:=$Alert_t+($CR*2)
	//$Alert_t:=$Alert_t+"Swapping to Blob."
	//ALERT($Alert_t)
	//
	//TEXT TO BLOB(◊Fnd_rVar_Declarations_t;◊FND_RVAR_DECLARATIONS_Blob;Mac text without length ;*)
	//◊Fnd_rVar_Declarations_t:=$CR
	//◊Fnd_rVar_TextOrBlob_i:=0
	//  ` CLOSE WINDOW
	//  ` ALERT("32,000 characters is the most a text variable can hold."+$CR+String(Length(◊Fnd_rVar_Declarations_t);"###,##0")+" characters have been written so far, a total of "+String($CurrentVar_i-◊Fnd_rVar_StartNumber_i;"###,##0")+" variable declarons.")
	//  `$CurrentVar_i:=◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i+1
	//Else 
	//TEXT TO BLOB(◊Fnd_rVar_Declarations_t;◊FND_RVAR_DECLARATIONS_Blob;Mac text without length ;*)
	//◊Fnd_rVar_Declarations_t:=$CR
	//End if 
	//End if 
	//End for 
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$CR+$Delimiter_s+($CR*2)
	//
	//Else 
	//
	//◊Fnd_rVar_Declarations_t:=""
	//$MaxDeclarations_i:=◊Fnd_rVar_NumberOfVars_i*20
	//For ($DataIndex_i;2;$NumberOfVariableTypes_i)  `  We ignore Alpha Field
	//If ($DataIndex_i=14)  `  Integer
	//Else 
	//
	//$DataType_i:=◊Fnd_VariableTypes_ai{$DataIndex_i}
	//$Prefix_s:=$Prefix_at{$DataIndex_i}
	//$Suffix_s:=$Suffix_at{$DataIndex_i}
	//
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$CR+"Fnd_rVar_VariableCount ("+String($DataType_i)+" ;"+String(◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i-1)+")"+$CR
	//For ($CurrentVar_i;◊Fnd_rVar_StartNumber_i;◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i-1)
	//If ($CurrentVar_i>999)
	//$CurrentVar_i:=◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i+1
	//Else 
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$Prefix_s+◊Fnd_rVar_Prefix_t+String($DataType_i)+"_"+String($CurrentVar_i;"000")+$Suffix_s+$CR
	//$ProgressCounter_i:=$ProgressCounter_i+1
	//ERASE WINDOW($WindowID)
	//GOTO XY(2;1)
	//MESSAGE("Declaration: "+String($ProgressCounter_i;"###,##0")+" of "+String($MaxDeclarations_i;"###,##0"))
	//End if 
	//
	//If (Length(◊Fnd_rVar_Declarations_t)>31800)
	//If (◊Fnd_rVar_TextOrBlob_i=1)
	//$Alert_t:="32,000 characters is the most a text variable can hold."+$CR
	//$Alert_t:=$Alert_t+String(Length(◊Fnd_rVar_Declarations_t);"###,##0")
	//$Alert_t:=$Alert_t+" characters have been written so far, a total of "
	//$Alert_t:=$Alert_t+String($CurrentVar_i-◊Fnd_rVar_StartNumber_i;"###,##0")+" variable declarations."
	//$Alert_t:=$Alert_t+($CR*2)
	//$Alert_t:=$Alert_t+"Swapping to Blob."
	//ALERT($Alert_t)
	//
	//TEXT TO BLOB(◊Fnd_rVar_Declarations_t;◊FND_RVAR_DECLARATIONS_Blob;Mac text without length ;*)
	//◊Fnd_rVar_Declarations_t:=$CR
	//◊Fnd_rVar_TextOrBlob_i:=0
	//  ` CLOSE WINDOW
	//  ` ALERT("32,000 characters is the most a text variable can hold."+$CR+String(Length(◊Fnd_rVar_Declarations_t);"###,##0")+" characters have been written so far, a total of "+String($CurrentVar_i-◊Fnd_rVar_StartNumber_i;"###,##0")+" variable declarons.")
	//  `$CurrentVar_i:=◊Fnd_rVar_StartNumber_i+◊Fnd_rVar_NumberOfVars_i+1
	//Else 
	//TEXT TO BLOB(◊Fnd_rVar_Declarations_t;◊FND_RVAR_DECLARATIONS_Blob;Mac text without length ;*)
	//◊Fnd_rVar_Declarations_t:=$CR
	//End if 
	//End if 
	//End for 
	//◊Fnd_rVar_Declarations_t:=◊Fnd_rVar_Declarations_t+$CR+$Delimiter_s+($CR*2)
	//
	//End if 
	//End for 
	//End if 
	//
	//
	//If (◊Fnd_rVar_TextOrBlob_i=1)
	//◊Fnd_rVar_Declarations_t:="`  Compiler Definitions automatically generated."+$CR+"`  Total # of characters: "+String(Length(◊Fnd_rVar_Declarations_t)+117;"###,##0")+$CR+$Delimiter_s+($CR*2)+◊Fnd_rVar_Declarations_t
	//Else 
	//TEXT TO BLOB(◊Fnd_rVar_Declarations_t;◊FND_RVAR_DECLARATIONS_Blob;Mac text without length ;*)
	//◊Fnd_rVar_Declarations_t:="`  Compiler Definitions automatically generated."+$CR+"`  Total # of bytes: "+String(BLOB size(◊FND_RVAR_DECLARATIONS_Blob);"###,##0")
	//End if 
	//
	//CLOSE WINDOW
	//Else 
	//ALERT("Please recheck your settings."+$CR+"Make certain at least one radio button is selected and you have entered a prefix.")
	//End if 
	
	
End if 
