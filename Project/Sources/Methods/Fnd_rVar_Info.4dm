//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_Info (label) --> Text

// Returns requested information.  See the Fnd_Gen_ComponentInfo method
//   for more information.

// Method Type: Protected

// Parameters: 
//   $1 : Text : Info desired

// Returns: 
//   $0 : Text : Response

//   Created by Wayne Stewart (10/05/2004)
//     waynestewart@mac.com
// ----------------------------------------------------

Fnd_rVar_Init

C_TEXT:C284($0;$1;$request_t;$reply_t)

$request_t:=$1

Case of 
	: ($request_t="name")
		$reply_t:="Reusable Variables"
		
	: ($request_t="version")
		$reply_t:="19.0"
		
	Else 
		$reply_t:="Fnd_LabelNotRecognized"
End case 

$0:=$reply_t
