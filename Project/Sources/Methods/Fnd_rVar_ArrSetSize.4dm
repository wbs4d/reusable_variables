//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ArrSetSize

// Used to set the size of an array
// regardless of the array type

// Access: Private

// Parameters: 
//   $1 : Pointer : Pointer to array to adjust in size
//   $2 : Integer : The new size of the array

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
// ----------------------------------------------------


If (False:C215)
	Fnd_rVar_ArrSetSize
End if 




// Parameters: 
C_POINTER:C301($1)  //  Pointer to array to resize
C_LONGINT:C283($2)  //  The new number of elements  

// Local Variables:  
C_TEXT:C284($Exception_t)
C_POINTER:C301($Array_ptr)
C_LONGINT:C283($NumberOfElements_i)
C_LONGINT:C283($VariableType_i)
C_LONGINT:C283($InitialSizeOfArray_i)


//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

$Array_ptr:=$1
$NumberOfElements_i:=$2
$VariableType_i:=Type:C295($1)

// Error Checking
// ----- --------
$Exception_t:=""

// Check for errors

Case of 
	: ($VariableType_i#Is pointer:K8:14)  //  #1  Only a pointer should be passed to this method. 
		//This error can only occur in interpreted mode
		$Exception_t:="Incorrect parameter type (not a pointer) passed."
		
	: (Not:C34(Fnd_rVar_IsAnArray(Type:C295($Array_ptr->))))  //  #2  If the pointer is not to an array then we have a stuff up
		$Exception_t:="Incorrect pointer type (not an array pointer) passed."
		
	: ($NumberOfElements_i<0)  //  #3  A negative number of elements were specified. 
		//  We will just use the absolute value rather than throw an exception
		$NumberOfElements_i:=Abs:C99($NumberOfElements_i)
End case 

If (Length:C16($Exception_t)=0)
	$InitialSizeOfArray_i:=Size of array:C274($Array_ptr->)
	Case of 
		: ($NumberOfElements_i=0)
			Fnd_rVar_ArrClear($Array_ptr)
		: ($NumberOfElements_i>$InitialSizeOfArray_i)
			Fnd_rVar_ArrAddElements($Array_ptr; $NumberOfElements_i-$InitialSizeOfArray_i)
		: ($NumberOfElements_i<$InitialSizeOfArray_i)
			Fnd_rVar_ArrRemoveElements($Array_ptr; $InitialSizeOfArray_i-$NumberOfElements_i)
	End case 
	
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_ArrSetSize"; $Exception_t)
End if 
