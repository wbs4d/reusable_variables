//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_DisplayUsageStats

// Opens a floating palette showing rVar usage

// Access: Shared

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com
// Modified by: Wayne Stewart (28/11/08)
//   Deleted 2nd method call

// ----------------------------------------------------

Fnd_rVar_Init

var $processNumber; $WindowID : Integer

If (Count parameters:C259=0)
	$processNumber:=New process:C317("Fnd_rVar_DisplayUsageStats"; 0; "$Reusable Variable Usage"; 1; *)
	BRING TO FRONT:C326($processNumber)
Else 
	$WindowID:=Open form window:C675("Usage Data"; Palette form window:K39:9; Horizontally centered:K39:1; Vertically centered:K39:4; *)
	SET WINDOW TITLE:C213("Usage Data")
	DIALOG:C40("Usage Data")
	CLOSE WINDOW:C154($WindowID)
	
End if 