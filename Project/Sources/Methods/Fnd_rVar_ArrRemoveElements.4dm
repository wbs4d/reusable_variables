//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ArrRemoveElements

// Used to remove elements from an array
// Removes 'n' elements from the end of an array
//     regardless of the array type

// Access: Private

// Parameters: 
//   $1 : Pointer : Pointer to array to decrease in size
//   $2 : Integer : The number of elements to remove

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
// ----------------------------------------------------

If (False:C215)
	Fnd_rVar_ArrRemoveElements
End if 



// Parameters:    None Used
C_POINTER:C301($1)  //  Pointer to array to decrease in size
C_LONGINT:C283($2)  //  The number of elements to remove  

// Local Variables:  
C_TEXT:C284($Exception_t)
C_POINTER:C301($Array_ptr)  //  
C_LONGINT:C283($NumberOfElements_i)
C_LONGINT:C283($VariableType_i)
C_LONGINT:C283($InitialSizeOfArray_i)

// Returns:    Nothing

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// Description
//   Removes 'n' elements from the end of an array
//     regardless of the array type
// ----------------------------------------------------

$Array_ptr:=$1
$NumberOfElements_i:=$2
$Exception_t:=""
$VariableType_i:=Type:C295($Array_ptr)

// Error Checking
// ----- --------
// Check for errors

Case of 
	: ($VariableType_i#Is pointer:K8:14)  //  #1  Only a pointer should be passed to this method. 
		//This error can only occur in interpreted mode
		$Exception_t:="Incorrect parameter type (not a pointer) passed."
		
	: (Not:C34(Fnd_rVar_IsAnArray(Type:C295($Array_ptr->))))  //  #2  If the pointer is not to an array then we have a stuff up
		$Exception_t:="Incorrect pointer type (not an array pointer) passed."
		
	: ($NumberOfElements_i<0)  //  #3  A negative number of elements were specified. 
		//  We will just use the absolute value rather than throw an exception
		$NumberOfElements_i:=Abs:C99($NumberOfElements_i)
End case 


If (Length:C16($Exception_t)=0)
	$InitialSizeOfArray_i:=Size of array:C274($Array_ptr->)
	//make sure not removing more than exists
	If (Not:C34($NumberOfElements_i>$InitialSizeOfArray_i))  //fine
		//remove the element(s)        
		DELETE FROM ARRAY:C228($Array_ptr->; $InitialSizeOfArray_i+1-$NumberOfElements_i; $NumberOfElements_i)
	Else   //  Just Clear the array
		If ($InitialSizeOfArray_i>0)  // Must check there are some elements in the array 
			DELETE FROM ARRAY:C228($Array_ptr->; 1; $InitialSizeOfArray_i)  // if you fail to do this an error will be generated
		End if 
	End if 
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_ArrRemoveElements"; $Exception_t)
	
End if 
