//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: aa_test

// Testing module

// Access: Private

// Created by Wayne Stewart (2023-03-01T13:00:00Z)

//     waynestewart@mac.com
// ----------------------------------------------------
If (False:C215)
	aa_test
End if 



C_LONGINT:C283($i)
Fnd_rVar_DisplayUsageStats
ARRAY POINTER:C280($Array_ptr; 500)
For ($i; 1; 200)
	$Array_ptr{$i}:=Fnd_rVar_GetVariableByType(Is BLOB:K8:12)
End for 

If (Is compiled mode:C492)
	ALERT:C41("First 200 allocated")
End if 

For ($i; 201; 400)
	$Array_ptr{$i}:=Fnd_rVar_GetVariableByType(Is integer:K8:5)
End for 
If (Is compiled mode:C492)
	ALERT:C41("Next 200 allocated")
End if 

For ($i; 401; 500)
	$Array_ptr{$i}:=Fnd_rVar_GetVariableByType(Is integer:K8:5)
End for 

If (Is compiled mode:C492)
	ALERT:C41("Last 100 allocated")
End if 


For ($i; 1; 500)
	Fnd_rVar_ReturnVariables($Array_ptr{$i})
End for 


If (Is compiled mode:C492)
	ALERT:C41("All vars returned")
End if 
