//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_GetChosenDataType
// Global and IP variables accessed:     
//   C_INTEGER(◊Fnd_rVar_ChosenType_i)

// Method Type:    Private

// Parameters:    
C_POINTER:C301($1)  //  Self Pointer

// Local Variables:     
C_TEXT:C284($varName_t)

// Returns:    Nothing

// Created by Wayne Stewart (11/09/2004)
//     waynestewart@mac.com
//  Sets the variable ◊Fnd_rVar_ChosenType_i according to the radio button pressed
// ----------------------------------------------------

$varName_t:=Fnd_rVar_GetVariableName($1)
<>Fnd_rVar_ChosenType_i:=Num:C11($varName_t)