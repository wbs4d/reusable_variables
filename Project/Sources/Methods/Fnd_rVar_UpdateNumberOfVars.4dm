//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Method: Fnd_rVar_UpdateNumberOfVars
// Global and IP variables accessed:     None Used

// Method Type:    Private

// Parameters:    None Used

// Local Variables:
C_LONGINT:C283($CurrentVarType_i)
// Returns:    Nothing

// Created by Wayne Stewart (6 Apr, 2006)
//     waynestewart@mac.com
//  Call this method if you need to enlarge (or contract) the rvar pool after
//   the component has been initialised

// Modified by: Wayne Stewart (23/10/07)
// 4D v11
// Although this will still function in a "matrix" database
//  it is inappropriate in a host database
// ----------------------------------------------------

Fnd_rVar_Init

Compiler_Fnd_ReusableVariables  //  This will declare the new vars

For ($CurrentVarType_i;1;Size of array:C274(<>Fnd_rVar_inUse_aBool);1)
	If (Fnd_rVar_IsValidVariableType($CurrentVarType_i))  //  Only do this if it is a valid type
		Fnd_rVar_ArrSetSize(-><>Fnd_rVar_inUse_aBool{$CurrentVarType_i};Fnd_rVar_VariableCount($CurrentVarType_i))
	End if 
End for 