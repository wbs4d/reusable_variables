//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: TS_DateAndTimeToISO (date{; time}) --> Text

// Returns an ISO 8601 formatted date or date-time value.
// If only a date is supplied, a calendar date is returned: YYYY-MM-DD
// If both date and time are supplied, a date-time value is returned: YYYY-MM-DDThh:mm:ss
// If both are passed but the date is !00/00/00! then only the time is returned: hh:mm:ss
// If neither are supplied the current date and time will be used

// For more information:
//   <http://www.iso.org/iso/en/prods-services/popstds/datesandtime.html>

// Access: Shared

// Parameters:
//   $1 : Date : A date (optional)
//   $2 : Time : A time (optional)

// Returns:
//   $0 : Text : The date-time stamp

// Created by Wayne Stewart
//     wayne@4dsupport.guru
// ----------------------------------------------------

C_TEXT:C284($0;$dateTimeStamp_t)
C_DATE:C307($1;$date_d)
C_TIME:C306($2;$time_h)
C_TEXT:C284($time_t;$hours_t;$minutes_t;$seconds_t)
C_LONGINT:C283($numberOfParameters_i)

$numberOfParameters_i:=Count parameters:C259
If ($numberOfParameters_i=0)
	$dateTimeStamp_t:=TS_DateAndTimeToISO(Current date:C33;Current time:C178)
Else 
	Case of 
		: ($numberOfParameters_i=1)
			$date_d:=$1
			$time_h:=?00:00:00?
			
		Else 
			$date_d:=$1
			$time_h:=$2
			
	End case 
	
	Case of 
		: ($date_d=!00-00-00!) & ($time_h=?00:00:00?)
			$dateTimeStamp_t:=""
			
		: ($date_d=!00-00-00!)
			
		: ($numberOfParameters_i=1)
			$date_d:=$1
			$time_h:=?00:00:00?
			
		Else 
			$date_d:=$1
			$time_h:=$2
			
	End case 
	
	$dateTimeStamp_t:=String:C10(Year of:C25($date_d);"0000")
	$dateTimeStamp_t:=$dateTimeStamp_t+"-"+String:C10(Month of:C24($date_d);"00")  //month
	$dateTimeStamp_t:=$dateTimeStamp_t+"-"+String:C10(Day of:C23($date_d);"00")  //day
	
	If (Count parameters:C259>=2)
		If ($date_d=!00-00-00!)
			$dateTimeStamp_t:=""
		Else 
			$dateTimeStamp_t:=$dateTimeStamp_t+"T"
		End if 
		
		$time_t:=String:C10($2;HH MM SS:K7:1)
		$hours_t:=Substring:C12($time_t;1;2)
		$minutes_t:=Substring:C12($time_t;4;2)
		$seconds_t:=Substring:C12($time_t;7;2)
		$dateTimeStamp_t:=$dateTimeStamp_t+$hours_t+":"+$minutes_t+":"+$seconds_t
	End if 
	//
	
End if 

$0:=$dateTimeStamp_t