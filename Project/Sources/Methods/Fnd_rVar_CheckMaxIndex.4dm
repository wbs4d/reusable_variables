//%attributes = {"invisible":true}

// ----------------------------------------------------
// Project Method: Fnd_rVar_CheckMaxIndex

// Method Type:    Private

// Parameters:

// Local Variables:

// Created by Wayne Stewart (Oct 23, 2007)
//     waynestewart@mac.com

//   

// ----------------------------------------------------


If (<>Fnd_rVar_NumberOfVars_i+<>Fnd_rVar_StartNumber_i>998)
	ALERT:C41("The maximum index value is 999, this would exceed that maximum.")
	GOTO OBJECT:C206(Self:C308->)
	HIGHLIGHT TEXT:C210(Self:C308->;1;MAXTEXTLENBEFOREV11:K35:3)
End if 