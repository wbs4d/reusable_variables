//%attributes = {"invisible":true,"shared":true}
// ------------------------------------------------------------
// Method: Util_ClearBigObject
//   Copyright ©Mac to Basics, 1989 - 2003.
//   All rights reserved.
// ------------------------------------------------------------
// Description
// Used to clear the contents of a large object from memory
//    
// Parameters
C_POINTER:C301($1)  //  Pointer to the object
// Interprocess Variable List
//  
// Global Variable List
//  
// Local Variables List
C_LONGINT:C283($Type;$Length;$NumberOfElements_i)
C_BOOLEAN:C305($Index;$NotCleared)
C_POINTER:C301($Variable_ptr)
// Modification History
// User name (OS): wbs
// Date and time: 19/11/03, 15:52:20
//
//============================================================
// Initialisation
//============================================================
//
// Parameters
// -----------------------------------------------------------
$Variable_ptr:=$1

// Local Variables
// -----------------------------------------------------------
// 
//============================================================
// method_code_begins_here


If (Not:C34(Is nil pointer:C315($Variable_Ptr)))
	If (Is a variable:C294($Variable_Ptr))
		If ((False:C215) & (Is compiled mode:C492))  //     If this is an interpreted database
			CLEAR VARIABLE:C89($Variable_Ptr->)  //  This doesn't appear to work, so blank it out
			$NotCleared:=False:C215
		Else 
			$Type:=Type:C295($Variable_Ptr->)
			$NotCleared:=True:C214
		End if 
	Else   //     This is a field
		GET FIELD PROPERTIES:C258($Variable_Ptr;$Type;$Index;$Length)
		$NotCleared:=True:C214
	End if 
End if 

If ($NotCleared)
	Case of 
		: ($Type=Is string var:K8:2) | \
			($Type=Is text:K8:3) | \
			($Type=Is alpha field:K8:1)  //     Alpha
			$Variable_Ptr->:=""
			
		: ($Type=Is integer:K8:5) | \
			($Type=Is longint:K8:6) | \
			($Type=Is real:K8:4) | \
			($Type=Is integer 64 bits:K8:25)  //     Numeric
			$Variable_Ptr->:=0
			
		: ($Type=Is BLOB:K8:12)  //     BLOB
			SET BLOB SIZE:C606($Variable_Ptr->;0)
			
		: ($Type=Is picture:K8:10)  //     Picture
			C_PICTURE:C286($Pict)
			$Variable_Ptr->:=$Pict
			
		: ($Type=Is date:K8:7)  //     Date
			$Variable_Ptr->:=!00-00-00!
			
		: ($Type=Is boolean:K8:9)  //     Boolean
			$Variable_Ptr->:=False:C215
			
		: ($Type=7)  //     SubFile
			Fnd_rVar_KillSubRecs($Variable_Ptr)
			
		: ($Type=Is time:K8:8)  //     Time
			$Variable_Ptr->:=?00:00:00?
			
		: ($Type=Is object:K8:27)  //     object
			$Variable_Ptr->:=JSON Parse:C1218("{}")
			
		: ($type=Real array:K8:17)  //     Real Array
			ARRAY REAL:C219($Variable_Ptr->;0)
			
		: ($type=Integer array:K8:18)  //     integer Array
			ARRAY INTEGER:C220($Variable_Ptr->;0)
			
		: ($type=LongInt array:K8:19)  //     Longint Array
			ARRAY LONGINT:C221($Variable_Ptr->;0)
			
		: ($type=Date array:K8:20)  //     Date Array
			ARRAY DATE:C224($Variable_Ptr->;0)
			
		: ($type=Text array:K8:16)  //     text Array
			ARRAY TEXT:C222($Variable_Ptr->;0)
			
		: ($type=Picture array:K8:22)  //     Picture Array
			ARRAY PICTURE:C279($Variable_Ptr->;0)
			
		: ($type=Pointer array:K8:23)  //     Pointer Array
			ARRAY POINTER:C280($Variable_Ptr->;0)
			
		: ($type=String array:K8:15)  //     String Array
			$NumberOfElements_i:=Size of array:C274($Variable_Ptr->)
			If ($NumberOfElements_i>0)
				DELETE FROM ARRAY:C228($Variable_Ptr->;1;$NumberOfElements_i)
			End if 
			
		: ($type=Boolean array:K8:21)  //     Boolean Array
			ARRAY BOOLEAN:C223($Variable_Ptr->;0)
			
		: ($type=Object array:K8:28)  //     Object Array
			ARRAY OBJECT:C1221($Variable_Ptr->;0)
			
		: ($type=Time array:K8:29)  //     time Array
			ARRAY TIME:C1223($Variable_Ptr->;0)
			
		: ($type=Blob array:K8:30)  //     BLOB Array
			ARRAY BLOB:C1222($Variable_Ptr->;0)
			
			
			
			
	End case 
End if 


// end_of_method