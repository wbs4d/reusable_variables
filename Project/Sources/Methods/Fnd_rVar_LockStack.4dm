//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_Lock_Stack

// Method Type:     Private

// Parameters:     None Used

// Local Variables:      None Used

// Returns:     Nothing

// Created by Wayne Stewart (14/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

While (Semaphore:C143(Fnd_rVar_StackSemaphore))
	IDLE:C311
	DELAY PROCESS:C323(Current process:C322;3)
End while 