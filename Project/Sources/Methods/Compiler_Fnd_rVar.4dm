//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Compiler_Fnd_rVar

// Compiler declarations for the component
// but not the actual reusable variables

// Access: Private

// Created by Wayne Stewart (12/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// ----------------------------------------------------

C_BOOLEAN:C305(<>Fnd_rVar_Initialised_b)
If (Not:C34(<>Fnd_rVar_Initialised_b))
	
	ARRAY BOOLEAN:C223(<>Fnd_rVar_inUse_aBool; 0; 0)
	
	//`````````````````````````````````````````````````````
	//  Declare blank variables & initialise
	C_POINTER:C301(<>Fnd_rVar_Blank_Ptr)
	
	ARRAY INTEGER:C220(<>Fnd_VariableTypes_ai; 0)
	ARRAY INTEGER:C220(<>Fnd_ArrayTypes_ai; 0)
	//C_LONGINT(<>Fnd_rVar_t30_Thermo;<>Fnd_rVar_t22_Thermo;<>Fnd_rVar_t17_Thermo;<>Fnd_rVar_t15_Thermo;<>Fnd_rVar_t16_Thermo;<>Fnd_rVar_t19_Thermo;<>Fnd_rVar_t20_Thermo;<>Fnd_rVar_t14_Thermo;<>Fnd_rVar_t21_Thermo;<>Fnd_rVar_t18_Thermo;<>Fnd_rVar_t6_Thermo;<>Fnd_rVar_t4_Thermo;<>Fnd_rVar_t9_Thermo;<>Fnd_rVar_t3_Thermo;<>Fnd_rVar_t23_Thermo;<>Fnd_rVar_t1_Thermo;<>Fnd_rVar_t24_Thermo;<>Fnd_rVar_t2_Thermo;<>Fnd_rVar_t11_Thermo)
	C_LONGINT:C283(<>Fnd_rVar_t30_Count; <>Fnd_rVar_t22_Count; <>Fnd_rVar_t17_Count; <>Fnd_rVar_t15_Count; <>Fnd_rVar_t16_Count; <>Fnd_rVar_t19_Count; <>Fnd_rVar_t20_Count; <>Fnd_rVar_t14_Count; <>Fnd_rVar_t21_Count; <>Fnd_rVar_t18_Count; <>Fnd_rVar_t6_Count; <>Fnd_rVar_t4_Count; <>Fnd_rVar_t9_Count; <>Fnd_rVar_t3_Count; <>Fnd_rVar_t23_Count; <>Fnd_rVar_t1_Count; <>Fnd_rVar_t24_Count; <>Fnd_rVar_t2_Count; <>Fnd_rVar_t11_Count)
	C_LONGINT:C283(<>Fnd_rVar_t30_Usage; <>Fnd_rVar_t22_Usage; <>Fnd_rVar_t17_Usage; <>Fnd_rVar_t15_Usage; <>Fnd_rVar_t16_Usage; <>Fnd_rVar_t19_Usage; <>Fnd_rVar_t20_Usage; <>Fnd_rVar_t14_Usage; <>Fnd_rVar_t21_Usage; <>Fnd_rVar_t18_Usage; <>Fnd_rVar_t6_Usage; <>Fnd_rVar_t4_Usage; <>Fnd_rVar_t9_Usage; <>Fnd_rVar_t3_Usage; <>Fnd_rVar_t23_Usage; <>Fnd_rVar_t1_Usage; <>Fnd_rVar_t24_Usage; <>Fnd_rVar_t2_Usage; <>Fnd_rVar_t11_Usage)
	C_LONGINT:C283(<>Fnd_rVarClose)
	
	ARRAY LONGINT:C221(<>Fnd_rVar_TypeCount_ai; 30)
	C_REAL:C285(<>Fnd_rVar_UpdateFreq_r)
	
	//`````````````````````````````````````````````````````
	C_LONGINT:C283(<>xb1; <>xb2)
	
	C_LONGINT:C283(<>Fnd_rVar_RB9)
	C_LONGINT:C283(<>Fnd_rVar_RB8)
	C_LONGINT:C283(<>Fnd_rVar_RB4)
	C_LONGINT:C283(<>Fnd_rVar_RB6)
	C_LONGINT:C283(<>Fnd_rVar_RB15)
	C_LONGINT:C283(<>Fnd_rVar_RB17)
	C_LONGINT:C283(<>Fnd_rVar_RB22)
	C_LONGINT:C283(<>Fnd_rVar_RB11)
	C_LONGINT:C283(<>Fnd_rVar_RB16)
	C_LONGINT:C283(<>Fnd_rVar_RB23)
	C_LONGINT:C283(<>Fnd_rVar_RB18)
	C_LONGINT:C283(<>Fnd_rVar_RB3)
	C_LONGINT:C283(<>Fnd_rVar_RB21)
	C_LONGINT:C283(<>Fnd_rVar_RB14)
	C_LONGINT:C283(<>Fnd_rVar_RB20)
	C_LONGINT:C283(<>Fnd_rVar_RB2)
	C_LONGINT:C283(<>Fnd_rVar_RB19)
	C_LONGINT:C283(<>Fnd_rVar_RB24)
	C_LONGINT:C283(<>Fnd_rVar_RB1)
	C_LONGINT:C283(<>Fnd_rVar_RB30)
	
	C_TEXT:C284(<>FND_RVAR_PREFIX_T)
	_O_C_INTEGER:C282(<>FND_RVAR_CHOSENTYPE_I)
	_O_C_INTEGER:C282(<>FND_RVAR_NUMBEROFVARS_I)
	C_TEXT:C284(<>FND_RVAR_DECLARATIONS_T)
	_O_C_INTEGER:C282(<>FND_RVAR_STRLENGTH_I)
	_O_C_INTEGER:C282(<>Fnd_rVar_StartNumber_i)
	
	C_TEXT:C284(<>Fnd_rVar_SliderHelp_t)
	
	C_BOOLEAN:C305(<>Fnd_rVar_Foundation_Present_b)
	
	
End if 


If (False:C215)  //  We don't want to run this
	C_POINTER:C301(Fnd_rVar_KillSubRecs; $1)
	C_POINTER:C301(Fnd_rVar_ResetValue; $1)
	
	C_POINTER:C301(Fnd_rVar_NullSetVariables; ${1})
	
	C_POINTER:C301(Fnd_rVar_GetVariableByType; $0)
	C_LONGINT:C283(Fnd_rVar_GetVariableByType; $1)
	
	C_POINTER:C301(Fnd_rVar_ReturnVariables; ${1})
	
	C_TEXT:C284(Fnd_rVar_GetVariableName; $0)
	C_POINTER:C301(Fnd_rVar_GetVariableName; $1)
	
	C_POINTER:C301(Fnd_rVar_ArrSetSize; $1)
	C_LONGINT:C283(Fnd_rVar_ArrSetSize; $2)
	
	C_BOOLEAN:C305(Fnd_rVar_IsAnArray; $0)
	C_LONGINT:C283(Fnd_rVar_IsAnArray; $1)
	
	C_POINTER:C301(Fnd_rVar_ArrClear; $1)
	
	C_POINTER:C301(Fnd_rVar_ArrAddElements; $1)
	C_LONGINT:C283(Fnd_rVar_ArrAddElements; $2)
	
	C_POINTER:C301(Fnd_rVar_ArrRemoveElements; $1)
	C_LONGINT:C283(Fnd_rVar_ArrRemoveElements; $2)
	
	C_LONGINT:C283(Fnd_rVar_VariableCount; $1; $2; $0)
	
	C_LONGINT:C283(Fnd_rVar_ResetAllVariables; $1)
	
	C_LONGINT:C283(Fnd_rVar_BuildVariableName; $1; $2)
	C_TEXT:C284(Fnd_rVar_BuildVariableName; $0)
	
	C_BOOLEAN:C305(Fnd_rVar_IsValidVariableType; $0)
	C_LONGINT:C283(Fnd_rVar_IsValidVariableType; $1)
	
	C_TEXT:C284(Fnd_rVar_Info; $1; $0)
	
	C_LONGINT:C283(Fnd_rVar_AdjustUsage; $1; $2)
	C_LONGINT:C283(Fnd_rVar_CalculateUsage; $1; $0)
	
	C_POINTER:C301(Fnd_rVar_GetChosenDataType; $1)  //  Self Pointer
	
	C_LONGINT:C283(Fnd_rVar_ConvertType; $1; $0)
	
	C_LONGINT:C283(Fnd_rVar_CrtVarDeclarations; $1)
	C_LONGINT:C283(Fnd_rVar_DisplayUsageStats; $1)
	
	C_TEXT:C284(Fnd_rVar_BugAlert; $1; $2)
	
	C_LONGINT:C283(Fnd_rVar_WriteDeclarations2; $1; $2; $3; $4; $5)
	C_TEXT:C284(Fnd_rVar_WriteDeclarations2; $6)
	C_POINTER:C301(Fnd_rVar_WriteDeclarations2; $7)
	
	C_LONGINT:C283(Fnd_rVar_BuildArrays; $1)
	C_POINTER:C301(Fnd_rVar_BuildArrays; $2; $3; $4)
	
	C_TEXT:C284(Fnd_FCS_WriteDocumentation; $1)
	C_BOOLEAN:C305(Fnd_FCS_WriteDocumentation; $2; $3)
	
	
	C_TEXT:C284(TS_DateAndTimeToISO; $0)
	C_DATE:C307(TS_DateAndTimeToISO; $1)
	C_TIME:C306(TS_DateAndTimeToISO; $2)
	
	C_TEXT:C284(Util_BuildComponent; $1)
	
End if 

