//%attributes = {"invisible":true,"shared":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ReturnVariables (pointer 'n' variables)

// Pass pointer to (1…n) reusable variables to return them to the pool

// Access: Shared

// Parameters: 
//   $1…n : Pointer : Pointer to reusable variables

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
// ----------------------------------------------------



// Parameters: 
C_POINTER:C301(${1})

// Local Variables:  
C_POINTER:C301($Variable_Ptr)
C_TEXT:C284($VariableName_t)
C_LONGINT:C283($VariableType_i; $Index_i; $NumberOfParameters_i; $CurrentParameter_i)

//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.
// ----------------------------------------------------


$NumberOfParameters_i:=Count parameters:C259

For ($CurrentParameter_i; 1; $NumberOfParameters_i)
	$Variable_Ptr:=${$CurrentParameter_i}
	
	$VariableName_t:=Fnd_rVar_GetVariableName($Variable_Ptr)  //get name of variable
	
	If (Length:C16($VariableName_t)#0)  //make sure name not empty
		$VariableType_i:=Type:C295($Variable_Ptr->)  //get type of variable
		
		If (Fnd_rVar_IsValidVariableType($VariableType_i))  //check validity of type
			
			If (Size of array:C274(<>Fnd_rVar_inUse_aBool{$VariableType_i})#0)  //make sure there are some variables of this type
				
				$Index_i:=Int:C8(Num:C11(Substring:C12($VariableName_t; Length:C16($VariableName_t)-2; 3)))  //get index to exact variable ` ### wbs Sep 18, 2004 - Change to allow for 3 digit #'s 001 -> 999
				
				If (($Index_i>0) & ($Index_i<=Size of array:C274(<>Fnd_rVar_inUse_aBool{$VariableType_i})))  //check validity of index
					
					Fnd_rVar_LockStack  //lock the DSS stack
					
					<>Fnd_rVar_inUse_aBool{$VariableType_i}{$Index_i}:=False:C215  //flag variable as no longer in use    
					
					Fnd_rVar_AdjustUsage($VariableType_i; -1)
					
					//clear the variable
					Case of 
						: (Is compiled mode:C492)
							CLEAR VARIABLE:C89($Variable_Ptr->)
							
						: (Fnd_rVar_IsAnArray(Type:C295($Variable_Ptr->)))
							Fnd_rVar_ArrClear($Variable_Ptr)
							
						Else 
							Fnd_rVar_NullSetVariables($Variable_Ptr)
					End case 
					
					Fnd_rVar_UnlockStack  //unlock the  stack
					
				Else   //ERROR
					// ### wbs 04/09/2004 - Disabling this error as it doesn't do any harm to pass an unexpected pointer
					// ### wbs 04/09/2004 - Being paranoid I will not delete the contents of the variable
					//`Fnd_rVar_BugAlert ("Fnd_rVar_ReturnVariables";"Referenced variable is invalid or out of range.")
				End if 
				//
			Else   //ERROR
				Fnd_rVar_BugAlert("Fnd_rVar_ReturnVariable"; "No variables matching type passed to method.")
				
			End if 
			//
		Else   //ERROR
			Fnd_rVar_BugAlert("Fnd_rVar_ReturnVariable"; "Type value is invalid or out of range.")
		End if 
		//
	Else   //ERROR
		Fnd_rVar_BugAlert("Fnd_rVar_ReturnVariable"; "Referenced variable failed to resolve.")
	End if 
	
End for 