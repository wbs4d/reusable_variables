//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_BuildVariableName (Variable Type; Index Number) --> ReturnType

// Returns the name of the variable being used

// Access: Private

// Parameters: 
//   $1 : Text : The variable type
//   $2 : Integer : The variable index #

// Returns: 
//   $0 : Text : The variable Name

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
// Modified by: wbs (17/5/04) Removed error checking - valid variable type is tested elsewhere
// ### Wayne Stewart Jun 17, 2005 - Tidied up method
// ### Wayne Stewart Aug 7, 2005 - Altered method so no longer able to get integers - now converted to longint

// ----------------------------------------------------

If (False:C215)
	Fnd_rVar_BuildVariableName
End if 



// ----------------------------------------------------
// Project Method: Fnd_rVar_BuildVariableName

// Returns the name of the variable being used

// Access: Proivate

// Parameters: 
//   $1 : Type : Description
//   $x : Type : Description (optional)

// Created by Wayne Stewart (2023-03-01T13:00:00Z)

//     waynestewart@mac.com
// ----------------------------------------------------
If (False:C215)
	Fnd_rVar_BuildVariableName
End if 



// ----------------------------------------------------
// Project Method: Fnd_rVar_BuildVariableName

// Method Type:    Private

// Parameters: 
C_LONGINT:C283($1)  //  The variable type
C_LONGINT:C283($2)  //  The variable index #

// Local Variables:  
C_LONGINT:C283($VariableType_i)  //  The variable type
C_LONGINT:C283($VariableIndex_i)  //  The variable index #


// Returns: 
C_TEXT:C284($0)  //  The variable name

// Created by Wayne Stewart (18/04/2004)
//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.
// Modified by: wbs (17/5/04)
//  Removed error checking - valid variable type is tested elsewhere
// ### Wayne Stewart Jun 17, 2005 - Tidied up method
// ### Wayne Stewart Aug 7, 2005 - Altered method so no longer able to get integers - now converted to longint
// ----------------------------------------------------

$VariableType_i:=$1
$VariableIndex_i:=$2

$VariableType_i:=Fnd_rVar_ConvertType($VariableType_i)

$0:="<>Fnd_rVar_t"+String:C10($VariableType_i)+"_"+String:C10($VariableIndex_i; "000")
