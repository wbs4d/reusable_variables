//%attributes = {"invisible":true}
// ----------------------------------------------------
// Project Method: Fnd_rVar_ArrClear

// Reduces an array to zero elements

// Access: Private

// Parameters: 
//   $1 : Pointer : Pointer to array to reduce to zero elements

// Created by Wayne Stewart (18/04/2004)

//     waynestewart@mac.com
// ----------------------------------------------------


C_POINTER:C301($1)  //  Pointer to array to clear  

// Local Variables:  
C_POINTER:C301($Array_ptr)
C_TEXT:C284($Exception_t)
C_LONGINT:C283($VariableType_i)
C_LONGINT:C283($InitialSizeOfArray_i)


//     waynestewart@mac.com
//   This component is based on the DSS library
//     first published by Deep Sky Technology on
//     the 4D Zine website.

// -----------------------------------------------------------

$Array_ptr:=$1
$Exception_t:=""
$VariableType_i:=Type:C295($Array_ptr)
// Error Checking
// ----- --------
Case of 
	: ($VariableType_i#Is pointer:K8:14)  //  #1  Only a pointer should be passed to this method.  This error can only occur in interpreted mode
		$Exception_t:="Incorrect parameter type (not a pointer) passed."
		
	: (Not:C34(Fnd_rVar_IsAnArray(Type:C295($Array_ptr->))))  //  #2  If the pointer is not to an array then we have a stuff up
		$Exception_t:="Incorrect pointer type (not an array pointer) passed."
		
End case 
// -----------------------------------------------------------
// 
//============================================================
// method_code_begins_here
If (Length:C16($Exception_t)=0)
	$InitialSizeOfArray_i:=Size of array:C274($Array_ptr->)
	If ($InitialSizeOfArray_i>0)  // Must check there are some elements in the array 
		DELETE FROM ARRAY:C228($Array_ptr->; 1; $InitialSizeOfArray_i)  // if you fail to do this an error will be generated
	End if 
Else 
	Fnd_rVar_BugAlert("Fnd_rVar_ArrClear"; $Exception_t)
End if 
// end_of_method
