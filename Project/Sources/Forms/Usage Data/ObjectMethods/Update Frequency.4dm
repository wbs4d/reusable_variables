If (<>Fnd_rVar_UpdateFreq_r=1)
	<>Fnd_rVar_SliderHelp_t:="Update frequency in seconds."+Char:C90(13)+"Currently set at "+String:C10(<>Fnd_rVar_UpdateFreq_r)+" second."
Else 
	If (<>Fnd_rVar_UpdateFreq_r=0)
		<>Fnd_rVar_UpdateFreq_r:=0.5
	End if 
	<>Fnd_rVar_SliderHelp_t:="Update frequency in seconds."+Char:C90(13)+"Currently set at "+String:C10(<>Fnd_rVar_UpdateFreq_r)+" seconds."
End if 
SET TIMER:C645(Round:C94(<>Fnd_rVar_UpdateFreq_r*60;0))