C_TIME:C306($DocRef)
C_TEXT:C284($DocPath_t)

_O_MAP FILE TYPES:C366("TEXT";"txt";"Plain Text Document")

$DocRef:=Create document:C266("";"TEXT")

If (OK=1)
	$DocPath_t:=Document
	SEND PACKET:C103($DocRef;<>Fnd_rVar_Declarations_t)  // Write one word in the document 
	CLOSE DOCUMENT:C267($DocRef)  // Close the document 
End if 

_O_SET DOCUMENT CREATOR:C531($DocPath_t;"R*ch")
_O_SET DOCUMENT TYPE:C530($DocPath_t;"TEXT")