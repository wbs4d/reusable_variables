
// ----------------------------------------------------
// User name (OS): Wayne Stewart
// Date and time: 23/10/07, 11:10:47
// ----------------------------------------------------
// Method: Form Method: CompilerDefinitions
// Description
// 
//
// Parameters
// ----------------------------------------------------
C_BOOLEAN:C305($Fnd_Wnd_CloseNow_b;<>Fnd_rVar_Foundation_Present_b;$Fnd_Gen_QuitNow_b)


Case of 
	: (Form event code:C388=On Load:K2:1)
		<>Fnd_rVar_Prefix_t:="<>Fnd_rVar_t"
		<>Fnd_rVar_ChosenType_i:=1
		<>Fnd_rVar_NumberOfVars_i:=10
		<>Fnd_rVar_StrLength_i:=255
		<>xb1:=1
		<>Fnd_rVar_ChosenType_i:=9
		<>Fnd_rVar_rb9:=1
		<>Fnd_rVar_StartNumber_i:=1
		SET WINDOW TITLE:C213("Reusable Variable Declaration Generator")
		
	: (Form event code:C388=On Close Box:K2:21)
		CANCEL:C270
		
	: (Form event code:C388=On Unload:K2:2)
		<>Fnd_rVar_Prefix_t:=""
		<>Fnd_rVar_Declarations_t:=""
		
	: (<>Fnd_rVar_Foundation_Present_b)
		EXECUTE METHOD:C1007("Fnd_Wnd_CloseNow";$Fnd_Wnd_CloseNow_b)
		If ($Fnd_Wnd_CloseNow_b)
			CANCEL:C270
		Else 
			EXECUTE METHOD:C1007("Fnd_Gen_QuitNow";$Fnd_Gen_QuitNow_b)
			If ($Fnd_Gen_QuitNow_b)
				CANCEL:C270
			End if 
		End if 
		
End case 